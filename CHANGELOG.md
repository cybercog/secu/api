# Changelog

All notable changes to `secusu` will be documented in this file.

## 2.0.2 - 2018-04-07

- ([#28](https://github.com/secusu/secusu/pull/28)) Fix bcrypt installing on CentOS

## 2.0.1 - 2016-06-20

- Sync Laravel with latest 5.2.x version
- Updated Telegram bot error message

## 2.0.0 - 2016-06-19

- ([#17](https://github.com/secusu/secusu/issues/17)) Chat rooms using websockets

## 1.4.0 - 2016-04-17

- Added websocket broadcasting
- Added optional bot's messages encryption

## 1.3.1 - 2016-04-13

- Hotfix `secu:destroy-outdated` command

## 1.3.0 - 2016-04-13

- Added SECU total created count

## 1.2.0 - 2016-04-13

- Added Telegram bot

## 1.1.0 - 2016-04-13

- Repository design pattern implemented
- Console command `secu:clean-outdated` renamed to `secu:destroy-outdated` 

## 1.0.1 - 2016-03-22

- Dropped PHP 5.5 support

## 1.0.0 - 2016-03-22

- initial release
